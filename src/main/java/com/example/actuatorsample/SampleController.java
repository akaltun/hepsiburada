package com.example.actuatorsample;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    @GetMapping("/helloworld")
    public String helloWorld() {

        return "Hello Hepsiburada from CAN AKALTUN";
    }
}
